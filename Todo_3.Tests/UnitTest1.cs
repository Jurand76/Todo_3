using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.Data.SqlClient;
using System.Transactions;
using Todo_3.Models;

namespace Todo_3.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void UserCreate()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            string connectionString = config.GetConnectionString("DefaultConnection");
            
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            string sqlQuery = "INSERT INTO Users (Name, Password, Email, Logged) " +
                                             "VALUES (@name, @password, @email, '0');";

            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            cmd.Parameters.AddWithValue("@name", "Userxxx1");
            cmd.Parameters.AddWithValue("@password", "Password1");
            cmd.Parameters.AddWithValue("@email", "abc@def.gh");

            cmd.ExecuteNonQuery();

            sqlQuery = "SELECT * FROM Users WHERE Name='Userxxx1' AND Email='abc@def.gh';";
            cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            Assert.AreEqual("Userxxx1", reader.GetString(1));
            Assert.AreEqual("abc@def.gh", reader.GetString(3));

            reader.Close();

            sqlQuery = "DELETE FROM Users WHERE Name='Userxxx1' AND Email='abc@def.gh';";
            cmd = new SqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            
            connection.Close();
        }

        [Test]
        public void TodoItemCreate()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            string connectionString = config.GetConnectionString("DefaultConnection");

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            string sqlQuery = "INSERT INTO TodoLists (UserId, ListName, Name, Description, DateStart, DateFinished, IsFinished, IsStarted) " +
                                                "VALUES (@userid, @listname, @name, @description, @datestart, @datefinished, @isfinished, @isstarted);";

            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            cmd.Parameters.AddWithValue("@userid", 99999);
            cmd.Parameters.AddWithValue("@listname", "Listxxxxx1");
            cmd.Parameters.AddWithValue("@name", "Itemxxxxx1");
            cmd.Parameters.AddWithValue("@description", "Desc");
            cmd.Parameters.AddWithValue("@datestart", "2022-01-01 00:00");
            cmd.Parameters.AddWithValue("@datefinished", "2022-01-02 00:00");
            cmd.Parameters.AddWithValue("@isfinished", "false");
            cmd.Parameters.AddWithValue("@isstarted", "false");

            cmd.ExecuteNonQuery();

            sqlQuery = "SELECT * FROM ToDoLists WHERE UserId = 99999 AND Name='Itemxxxxx1';";
            cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            Assert.AreEqual("Listxxxxx1", reader.GetString(2));
            Assert.AreEqual(false, reader.GetBoolean(7));

            reader.Close();

            sqlQuery = "DELETE FROM ToDoLists WHERE Name='Itemxxxxx1';";
            cmd = new SqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();

            connection.Close();
        }
    }
}