﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Todo_3.Models;
using Todo_3.Stats;

namespace Todo_3.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public Users user = new Users();
        public int UserId = 0;
        public string UserName = "";
        public DateTime date = DateTime.Now;
        public Statistics statistics = new Statistics();
        public List<TodoItem> reminder = new List<TodoItem>();

        public IndexModel(ILogger<IndexModel> logger) 
        {
            _logger = logger;
        }

        public void OnGet()
        {
            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();

                string sqlQuery = "SELECT Id, Name, Password, Email, Logged FROM Users WHERE Logged='1';";

                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                { 
                    user.Id = reader.GetInt32(0);
                    user.Name = reader.GetString(1);
                    user.Password = reader.GetString(2);
                    user.Email = reader.GetString(3);
                    user.Logged = reader.GetBoolean(4);
                    UserName = user.Name;
                    UserId = user.Id;
                }
                               
                connection.Close();
            }
            catch (Exception ex)
            {
                UserName = "";
            }
                        
            reminder = GetReminder.GetReminderList(UserId);

            try
            {
                statistics.TotalCountOfDoneTodos = GetStatistics.GetTotalCountOfDoneTodos(UserId);
                statistics.TotalCountOfStartedTodos = GetStatistics.GetTotalCountOfStartedTodos(UserId);
                statistics.TotalCountOfTodos = GetStatistics.GetTotalCountOfTodos(UserId);
                statistics.TotalCountOfExpireStartedTodos = GetStatistics.GetTotalCountOfExpiredStartTodos(date, UserId);
                statistics.TotalCountOfExpireFinishedTodos = GetStatistics.GetTotalCountOfExpiredFinishTodos(date, UserId);
            }
            catch
            {

            }
        }
    }
}
