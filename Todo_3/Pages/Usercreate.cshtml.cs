using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using System;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Pages
{
    
    public class UsercreateModel : PageModel
    {
        public Users item = new Users();
        public string errorMessage = "";
        public string successMessage = "";
        public readonly IConfiguration configuration;
        public void OnGet()
        {
           
        }

        public void OnPost()
        {
            item.Name = Request.Form["name"];
            item.Password = Request.Form["password"];
            item.Email = Request.Form["description"];
          
            if (String.IsNullOrEmpty(item.Name) || String.IsNullOrEmpty(item.Password) || String.IsNullOrEmpty(item.Email))
            {
                errorMessage = "Input proper values - Name, password and email cannot be empty";
                return;
            }

            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();

                string sqlQuery = "INSERT INTO Users (Name, Password, Email, Logged) " +
                                                 "VALUES (@name, @password, @email, '0');";

                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                cmd.Parameters.AddWithValue("@name", item.Name);
                cmd.Parameters.AddWithValue("@password", item.Password);
                cmd.Parameters.AddWithValue("@email", item.Email);
                
                cmd.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

            successMessage = "New item added correctly";
            Response.Redirect("Users");
        }
    }
}