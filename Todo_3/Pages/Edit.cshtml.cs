using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Pages
{
    public class EditModel : PageModel
    {
        public TodoItem item = new TodoItem();
        public string errorMessage = "";
        public string successMessage = "";
        public string selectedList = "";

        public void OnGet()
        {
            string id = Request.Query["id"];
            selectedList = Request.Query["selectedlist"];

            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();
                string sqlQuery = "SELECT * FROM TodoLists WHERE Id='" + id + "';";
                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    item.Id = reader.GetInt32(0);
                    item.UserId = reader.GetInt32(1);
                    item.ListName = reader.GetString(2);
                    item.Name = reader.GetString(3);
                    item.Description = reader.GetString(4);
                    item.DateStart = reader.GetDateTime(5);
                    item.DateFinished = reader.GetDateTime(6);
                    item.IsFinished = reader.GetBoolean(7);
                    item.IsStarted = reader.GetBoolean(8);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
        }

        public void OnPost()
        {
            selectedList = Request.Query["selectedlist"];
            item.Id = Convert.ToInt32(Request.Form["id"]);
            item.UserId = Convert.ToInt32(Request.Form["userid"]);
            item.ListName = Request.Form["listname"];
            item.Name = Request.Form["name"];
            item.Description = Request.Form["description"];
            item.DateStart = Convert.ToDateTime(Request.Form["datestart"]);
            item.DateFinished = Convert.ToDateTime(Request.Form["datefinished"]);
            if (Request.Form["isfinished"] == "1")
            {
                item.IsFinished = true;
            }
            else
            {
                item.IsFinished = false;
            }
            if (Request.Form["isstarted"] == "1")
            {
                item.IsStarted = true;
            }
            else
            {
                item.IsStarted = false;
            }

            if (item.IsFinished)
                item.IsStarted = true;

            if (!(item.Name != "" && DateTime.TryParse(Request.Form["datestart"], out DateTime theDate) && DateTime.TryParse(Request.Form["datefinished"], out DateTime theDate2)))
            {
                errorMessage = "Input proper values - Name cannot be empty, Date should be at date format (2022-12-14 13:51 for example)";
                return;
            }

            if (item.DateStart > item.DateFinished)
            {
                errorMessage = "Start Date cannot be newer than Finish Date";
                return;
            }

            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();

                string sqlQuery = "UPDATE TodoLists SET Name=@name, ListName=@listname, UserId=@userid, Description=@description, DateStart=@datestart, DateFinished=@datefinished, IsFinished=@isfinished, IsStarted=@isstarted WHERE Id=@id";

                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                cmd.Parameters.AddWithValue("@id", item.Id);
                cmd.Parameters.AddWithValue("@userid", item.UserId);
                cmd.Parameters.AddWithValue("@listname", item.ListName);
                cmd.Parameters.AddWithValue("@name", item.Name);
                cmd.Parameters.AddWithValue("@description", item.Description);
                cmd.Parameters.AddWithValue("@datestart", item.DateStart);
                cmd.Parameters.AddWithValue("@datefinished", item.DateFinished);
                cmd.Parameters.AddWithValue("@isfinished", item.IsFinished);
                cmd.Parameters.AddWithValue("@isstarted", item.IsStarted);

                cmd.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

            if (String.IsNullOrEmpty(selectedList))
            {
                Response.Redirect("Lists?User_Id=" + item.UserId);
            }
            else
            {
                Response.Redirect("Listselected?User_Id=" + item.UserId + "&selectedlist=" + item.ListName);
            }
        }
    }
}
