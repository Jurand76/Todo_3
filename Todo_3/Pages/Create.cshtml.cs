using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using System;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Pages
{
    public class CreateModel : PageModel
    {
        public TodoItem item = new TodoItem();
        public string errorMessage = "";
        public string successMessage = "";
        public readonly IConfiguration configuration;
        public void OnGet()
        {
            item.DateStart = DateTime.Now;
            item.DateFinished = DateTime.Now;
            item.ListName = Request.Query["selectedlist"];
        }

        public void OnPost()
        {
            item.UserId = Convert.ToInt32(Request.Query["User_Id"]); ;
            if (String.IsNullOrEmpty(Request.Query["selectedlist"]))
            {
                item.ListName = Request.Form["listname"];
            }
            else
            {
                item.ListName = Request.Query["selectedlist"];
            }
            item.Name = Request.Form["name"];
            item.Description = Request.Form["description"];
            item.DateStart = Convert.ToDateTime(Request.Form["datestart"]);
            item.DateFinished = Convert.ToDateTime(Request.Form["datefinished"]);
            if (Request.Form["isstarted"] != "1")
            {
                item.IsStarted = false;
            }
            else
            {
                item.IsStarted = true;
            }
            if (Request.Form["isfinished"] != "1")
            {
                item.IsFinished = false;
            }
            else
            {
                item.IsFinished = true;
            }

            if (item.IsFinished)
                item.IsStarted = true;

            if (!(item.Name != "" && DateTime.TryParse(Request.Form["datestart"], out DateTime theDate) && DateTime.TryParse(Request.Form["datefinished"], out DateTime theDate2)))
            {
                errorMessage = "Input proper values - Name cannot be empty, Date should be at date format (2022-12-14 13:51 for example)";
                return;
            }

            if (item.DateStart > item.DateFinished)
            {
                errorMessage = "Start Date cannot be newer than Finish Date";
                return;
            }

            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();

                string sqlQuery = "INSERT INTO TodoLists (UserId, ListName, Name, Description, DateStart, DateFinished, IsFinished, IsStarted) " +
                                                 "VALUES (@userid, @listname, @name, @description, @datestart, @datefinished, @isfinished, @isstarted);";

                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                cmd.Parameters.AddWithValue("@userid", item.UserId);
                cmd.Parameters.AddWithValue("@listname", item.ListName);
                cmd.Parameters.AddWithValue("@name", item.Name);
                cmd.Parameters.AddWithValue("@description", item.Description);
                cmd.Parameters.AddWithValue("@datestart", item.DateStart);
                cmd.Parameters.AddWithValue("@datefinished", item.DateFinished);
                cmd.Parameters.AddWithValue("@isfinished", item.IsFinished);
                cmd.Parameters.AddWithValue("@isstarted", item.IsStarted);

                cmd.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

            successMessage = "New item added correctly";
            if (String.IsNullOrEmpty(Request.Query["selectedlist"]))
            {
                Response.Redirect("/Lists?User_Id=" + item.UserId);
            }
            else
            {
                Response.Redirect("/Listselected?User_Id=" + item.UserId + "&selectedlist=" + Request.Query["selectedlist"]);
            }
        }
    }
}
