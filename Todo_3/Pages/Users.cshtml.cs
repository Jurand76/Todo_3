using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Pages
{
    public class UsersModel : PageModel
    {
        public List<Users> users = new List<Users>();
        public int User_Id = 0;
        public void OnGet()
        {
            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();
               
                string sqlQuery = "SELECT * FROM Users;";
                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Users item = new Users();
                        item.Id = reader.GetInt32(0);
                        item.Name = reader.GetString(1);
                        item.Password = reader.GetString(2);
                        item.Email = reader.GetString(3);
                        item.Logged = reader.GetBoolean(4);

                        users.Add(item);
                    }
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                 return;
            }
        }
    }
}
