using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Data.SqlClient;
using Todo_3.Models;


namespace Todo_3.Pages
{
    public class UsereditModel : PageModel
    {
        public Users item = new Users();
        public string errorMessage = "";
        public string successMessage = "";
       
        public void OnGet()
        {
            string id = Request.Query["id"];
            
            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();
                string sqlQuery = "SELECT * FROM Users WHERE Id='" + id + "';";
                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    item.Id = reader.GetInt32(0);
                    item.Name = reader.GetString(1);
                    item.Password = reader.GetString(2);
                    item.Email = reader.GetString(3);
                    item.Logged = reader.GetBoolean(4);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
        }

        public void OnPost()
        {
            item.Id = Convert.ToInt32(Request.Form["id"]);
            item.Name = Request.Form["name"];
            item.Password = Request.Form["password"];
            item.Email = Request.Form["email"];
            if (Request.Form["logged"] == "1")
            {
                item.Logged = true;
            }
            else
            {
                item.Logged = false;
            }
          
            if (String.IsNullOrEmpty(item.Name) || String.IsNullOrEmpty(item.Password) || String.IsNullOrEmpty(item.Email))
            {
                errorMessage = "Input proper values - Name, password and email cannot be empty";
                return;
            }

            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();

                string sqlQuery = "UPDATE Users SET Name=@name, Password=@password, Email=@email, Logged=@logged WHERE Id=@id";

                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                cmd.Parameters.AddWithValue("@id", item.Id);
                cmd.Parameters.AddWithValue("@name", item.Name);
                cmd.Parameters.AddWithValue("@password", item.Password);
                cmd.Parameters.AddWithValue("@email", item.Email);
                cmd.Parameters.AddWithValue("@logged", item.Logged);
                
                cmd.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

            Response.Redirect("Users");
        }
    }
}
