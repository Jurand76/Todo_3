using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Pages
{
    public class ListsModel : PageModel
    {
        public List<TodoItem> todos = new List<TodoItem>();
        public string errorMessage = "";
        public int User_Id;
        public string filtering = "";
        public string hidden = "";

        public void OnGet()
        {
            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();
                User_Id = Convert.ToInt32(Request.Query["User_Id"]);
                filtering = Request.Query["filtering"];
                string sorting = "";
                hidden = Request.Query["hidden"];

                if (filtering != "")
                {
                    switch (filtering)
                    {
                        case "listname":
                            sorting = "ORDER BY ListName";
                            break;

                        case "itemname":
                            sorting = "ORDER BY TodoLists.Name";
                            break;

                        case "timestart":
                            sorting = "ORDER BY DateStart";
                            break;

                        case "timefinish":
                            sorting = "ORDER BY DateFinished";
                            break;

                        default:
                            sorting = "";
                            break;
                    }
                }

                string sqlQuery;

                if (String.IsNullOrEmpty(hidden))
                {
                    sqlQuery = "SELECT * FROM TodoLists JOIN Users ON TodoLists.UserId = Users.Id WHERE UserId = " + Request.Query["User_Id"] + " " + sorting;
                }
                else
                {
                    sqlQuery = "SELECT* FROM TodoLists JOIN Users ON TodoLists.UserId = Users.Id WHERE IsFinished='0' AND UserId = " + Request.Query["User_Id"] + " " + sorting;
                }

                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TodoItem item = new TodoItem();
                        item.Id = reader.GetInt32(0);
                        item.UserId = reader.GetInt32(1);
                        item.ListName = reader.GetString(2);
                        item.Name = reader.GetString(3);
                        item.Description = reader.GetString(4);
                        item.DateStart = reader.GetDateTime(5);
                        item.DateFinished = reader.GetDateTime(6);
                        item.IsFinished = reader.GetBoolean(7);
                        item.IsStarted = reader.GetBoolean(8);

                        todos.Add(item);
                        User_Id = item.UserId;
                    }
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
        }
    }
}

