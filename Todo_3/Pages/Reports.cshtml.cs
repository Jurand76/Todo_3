using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using Todo_3.Models;
using Todo_3.Stats;

namespace Todo_3.Pages
{
    public class ReportsModel : PageModel
    {
        public List<UsersReport> user_alltodos = new List<UsersReport>();
        public List<UsersReport> user_finishedtodos = new List<UsersReport>();
        public void OnGet()
        {
            int[] count = new int[5];
            string[] name = new string[5];
               
            user_alltodos = GetReport.GetUserWithMaxCountOfTodos();
            user_finishedtodos = GetReport.GetUserWithMaxCountOfFinishedTodos();
        }
    }
}
