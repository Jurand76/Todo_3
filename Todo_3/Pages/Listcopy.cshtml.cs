using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Pages
{
    public class ListcopyModel : PageModel
    {
        public List<TodoItem> todos = new List<TodoItem>();
        public string errorMessage = "";
        public int User_Id;
        public string filtering = "";
        public string listSelected = "";
        public void OnGet()
        {
            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();
                listSelected = Request.Query["selectedlist"];

                string sqlQuery = "SELECT * FROM TodoLists JOIN Users ON TodoLists.UserId = Users.Id WHERE UserId = " + Request.Query["User_Id"] + " AND ListName = '" + listSelected + "' " + filtering;
                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TodoItem item = new TodoItem();
                        item.Id = reader.GetInt32(0);
                        item.UserId = reader.GetInt32(1);
                        item.ListName = reader.GetString(2);
                        item.Name = reader.GetString(3);
                        item.Description = reader.GetString(4);
                        item.DateStart = reader.GetDateTime(5);
                        item.DateFinished = reader.GetDateTime(6);
                        item.IsFinished = reader.GetBoolean(7);
                        item.IsStarted = reader.GetBoolean(8);

                        todos.Add(item);
                        User_Id = item.UserId;
                    }
                }
                reader.Close();


                foreach (TodoItem copyitem in todos)
                {
                    sqlQuery = "INSERT INTO TodoLists (UserId, ListName, Name, Description, DateStart, DateFinished, IsFinished, IsStarted) "
                             + "VALUES (@userid, @listname, @name, @description, @datestart, @datefinished, @isfinished, @isstarted);";

                    cmd = new SqlCommand(sqlQuery, connection);

                    cmd.Parameters.AddWithValue("@userid", copyitem.UserId);
                    cmd.Parameters.AddWithValue("@listname", copyitem.ListName + " copy");
                    cmd.Parameters.AddWithValue("@name", copyitem.Name);
                    cmd.Parameters.AddWithValue("@description", copyitem.Description);
                    cmd.Parameters.AddWithValue("@datestart", copyitem.DateStart);
                    cmd.Parameters.AddWithValue("@datefinished", copyitem.DateFinished);
                    cmd.Parameters.AddWithValue("@isfinished", copyitem.IsFinished);
                    cmd.Parameters.AddWithValue("@isstarted", copyitem.IsStarted);

                    cmd.ExecuteNonQuery();
                }

                connection.Close();

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

            Response.Redirect("Listselected?User_Id=" + User_Id + "&selectedlist=" + listSelected + " copy");
        }
    }
}
