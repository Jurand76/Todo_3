using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Pages
{
    public class ListselectedModel : PageModel
    {
        public List<TodoItem> todos = new List<TodoItem>();
        public string errorMessage = "";
        public int User_Id;
        public string filtering = "";
        public string listSelected = "";

        public void OnGet()
        {
            try
            {
                SqlConnection connection = Database.GetConnection();
                connection.Open();
                filtering = Request.Query["filtering"];
                listSelected = Request.Query["selectedlist"];
                if (filtering != "")
                {
                    switch (filtering)
                    {
                        case "listname":
                            filtering = "ORDER BY ListName";
                            break;

                        case "itemname":
                            filtering = "ORDER BY TodoLists.Name";
                            break;

                        case "timestart":
                            filtering = "ORDER BY DateStart";
                            break;

                        case "timefinish":
                            filtering = "ORDER BY DateFinished";
                            break;

                        default:
                            filtering = "";
                            break;
                    }
                }

                string sqlQuery = "SELECT * FROM TodoLists JOIN Users ON TodoLists.UserId = Users.Id WHERE UserId = " + Request.Query["User_Id"] + " AND ListName = '" +listSelected + "' " + filtering;
                SqlCommand cmd = new SqlCommand(sqlQuery, connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TodoItem item = new TodoItem();
                        item.Id = reader.GetInt32(0);
                        item.UserId = reader.GetInt32(1);
                        item.ListName = reader.GetString(2);
                        item.Name = reader.GetString(3);
                        item.Description = reader.GetString(4);
                        item.DateStart = reader.GetDateTime(5);
                        item.DateFinished = reader.GetDateTime(6);
                        item.IsFinished = reader.GetBoolean(7);
                        item.IsStarted = reader.GetBoolean(8);

                        todos.Add(item);
                        User_Id = item.UserId;
                    }
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
        }
    }
}

