﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Stats
{
    public static class GetReminder
    {
        public static List<TodoItem> GetReminderList(int User_Id)
        {

            List<TodoItem> reminderTodos = new List<TodoItem>();
            var dateAndTime = DateTime.Now;
            var date = dateAndTime.Date;
           
            SqlConnection connection = Database.GetConnection();
            connection.Open();

            string sqlQuery = "SELECT * FROM TodoLists WHERE UserId='"+User_Id+"' AND IsFinished=0;";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
                {
                    TodoItem item = new TodoItem();
                    DateTime checkdate1;
                    DateTime checkdate2;

                    checkdate1 = reader.GetDateTime(5).Date;
                    checkdate2 = reader.GetDateTime(6).Date;

                    if (checkdate1 == date || checkdate2 == date)
                    {
                        item.Id = reader.GetInt32(0);
                        item.UserId = reader.GetInt32(1);
                        item.ListName = reader.GetString(2);
                        item.Name = reader.GetString(3);
                        item.Description = reader.GetString(4);
                        item.DateStart = reader.GetDateTime(5);
                        item.DateFinished = reader.GetDateTime(6);
                        item.IsFinished = reader.GetBoolean(7);
                        item.IsStarted = reader.GetBoolean(8);
                        reminderTodos.Add(item);
                    }
                }
                       
            reader.Close();
            connection.Close();
            return reminderTodos;
        }
    }
}

