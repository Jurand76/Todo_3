﻿using System;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Stats
{
    public static class GetStatistics
    {
        public static int GetTotalCountOfTodos(int User_Id)
        {
            int result;
            SqlConnection connection = Database.GetConnection();
            connection.Open();

            string sqlQuery = "SELECT COUNT(Id) FROM TodoLists WHERE UserId = '" + User_Id + "'";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            result = GetReaderResult(reader);

            reader.Close();
            connection.Close();
            return result;
        }

        public static int GetTotalCountOfDoneTodos (int User_Id)
        {
            int result;
            SqlConnection connection = Database.GetConnection();
            connection.Open();
            
            string sqlQuery = "SELECT COUNT(Id) FROM TodoLists WHERE IsFinished = '1' AND UserId = '" + User_Id + "';";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            result = GetReaderResult(reader);

            reader.Close();
            connection.Close();
            return result;
        }

        public static int GetTotalCountOfStartedTodos (int User_Id)
        {
            int result;
            SqlConnection connection = Database.GetConnection();
            connection.Open();
            
            string sqlQuery = "SELECT COUNT(Id) FROM TodoLists WHERE IsStarted = '1' AND UserId = '" + User_Id + "';";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            result = GetReaderResult(reader);

            reader.Close();
            connection.Close();
            return result;
        }

        public static int GetTotalCountOfExpiredStartTodos (DateTime date, int User_Id)
        {
            int result;
            SqlConnection connection = Database.GetConnection();
            connection.Open();

            string sqlQuery = "SELECT COUNT(Id) FROM TodoLists WHERE IsStarted = '0' AND DateStart < '" + date + "' AND UserId = '" + User_Id + "';";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            result = GetReaderResult(reader);

            reader.Close();
            connection.Close();
            return result;
        }

        public static int GetTotalCountOfExpiredFinishTodos(DateTime date, int User_Id)
        {
            int result;
            SqlConnection connection = Database.GetConnection();
            connection.Open();

            string sqlQuery = "SELECT COUNT(Id) FROM TodoLists WHERE IsFinished = '0' AND DateFinished < '" + date + "' AND UserId = '" + User_Id + "';";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            result = GetReaderResult(reader);
            
            reader.Close();
            connection.Close();
            return result;
        }

        public static int GetReaderResult (SqlDataReader reader)
        {
            int result;
            if (reader.Read())
            {
                result = reader.GetInt32(0);
            }
            else
            {
                result = 0;
            }
            return result;
        }
    }
}
