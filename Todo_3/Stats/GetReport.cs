﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Todo_3.Models;

namespace Todo_3.Stats
{
    public static class GetReport
    {
        public static List<UsersReport> GetUserWithMaxCountOfTodos()
        {
            List<UsersReport> usersList = new List<UsersReport>();
            
            SqlConnection connection = Database.GetConnection();
            connection.Open();

            string sqlQuery = "SELECT Users.Name, COUNT(TodoLists.Id) from TodoLists JOIN Users ON Users.Id = TodoLists.UserId GROUP BY Users.Name ORDER BY Users.Name DESC";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                UsersReport item = new UsersReport();
                item.Name = reader.GetString(0);
                item.Count = reader.GetInt32(1);
                usersList.Add(item);
            }
                    
            reader.Close();
            connection.Close();

            return usersList;
        }

        public static List<UsersReport> GetUserWithMaxCountOfFinishedTodos()
        {
            List<UsersReport> usersList = new List<UsersReport>();

            SqlConnection connection = Database.GetConnection();
            connection.Open();

            string sqlQuery = "SELECT Users.Name, COUNT(TodoLists.Id) from TodoLists JOIN Users ON Users.Id = TodoLists.UserId WHERE IsFinished='1' AND IsStarted='1' GROUP BY Users.Name ORDER BY Users.Name DESC";
            SqlCommand cmd = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                UsersReport item = new UsersReport();
                item.Name = reader.GetString(0);
                item.Count = reader.GetInt32(1);
                usersList.Add(item);
            }

            reader.Close();
            connection.Close();
            
            return usersList;
        }
    }
}
