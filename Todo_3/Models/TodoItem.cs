﻿using System;

namespace Todo_3.Models
{
    public class TodoItem
    { 
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ListName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateFinished { get; set; }
        public bool IsStarted { get; set; }
        public bool IsFinished { get; set; }
    }
}
