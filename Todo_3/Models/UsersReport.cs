﻿namespace Todo_3.Models
{
    public class UsersReport
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
