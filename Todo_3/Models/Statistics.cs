﻿namespace Todo_3.Models
{
    public class Statistics
    {
        public int TotalCountOfTodos { get; set; }
        public int TotalCountOfDoneTodos { get; set; }
        public int TotalCountOfStartedTodos { get; set; }
        public int TotalCountOfExpireStartedTodos { get; set; }
        public int TotalCountOfExpireFinishedTodos { get; set; }
    }
}
